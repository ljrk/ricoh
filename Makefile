.POSIX:

CC=c99
CFLAGS=-Wall -Wpedantic -Wno-deprecated-declarations -O2
LDLIBS=-ldl

CFLAGS_cups=`cups-config --cflags`
LDFLAGS_cups=`cups-config --ldflags`
LDLIBS_cups=`cups-config --libs` -lcupsimage

CFLAGS += $(CFLAGS_cups)
LDFLAGS += $(LDFLAGS_cups)
LDLIBS += $(LDLIBS_cups)

all: filter

.SUFFIXES:
.SUFFIXES: .c .o

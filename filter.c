#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <dlfcn.h>
#include <cups/cups.h>
#include <cups/ppd.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>

#define LIB "/opt/RICOH/lib/"
#define MODEL "RICOH SP 150"

static void SIGPIPE_handler(int sign)
{
	(void)sign;
}


static void SIGALRM_handler(int sign)
{
	(void)sign;
}

static void SIGTERM_handler(int sign)
{
	(void)sign;
}

#define INFO "INFO: "
#define ERROR "ERROR: "
#define CKP "check point #"

#define LOGFILE "/tmp/ricoh_filter.log"
static void printlog(const char *const logmsg)
{
	FILE *const logf = fopen(LOGFILE, "a+");
	fprintf(stderr, "%s\n", logmsg);
	fprintf(logf, "%s\n", logmsg);
	fclose(logf);
}

static int open_raster_image(const char *const filename)
{
	const int fd = open(filename, 0);
	if (fd == -1) {
		perror(ERROR "Unable to open raster file - ");
		sleep(1);
		exit(1);
	}
	return (fd);
}

struct {
	void *NTDCMS;
	void *InitNTDCMS;
	void *CreateNTDCMS;
	void *EndNTDCMS;
	void *StartColorAdjust;
	void *StartAdjustJob;
	void *UpdateScreenThread;
	void *pfUpdateScreenThread;
	void *UpdateScreenTRC;
	void *UpdateScreenTonerTRC;
} DllInterface;

static void InitDllInterface(void)
{
	static void *dl_handle;

	if (dl_handle) {
		dlclose(dl_handle);
		dl_handle = NULL;
	}
	if (!dl_handle) {
		dl_handle = dlopen(LIB MODEL "SUcl.so", RTLD_LAZY);
	}
	if (!dl_handle) {
		dl_handle = dlopen(LIB MODEL "cl.so", RTLD_LAZY);
	}
	if (!dl_handle) {
		dl_handle = dlopen(LIB MODEL "SUwcl.so", RTLD_LAZY);
	}
	if (!dl_handle) {
		dl_handle = dlopen(LIB MODEL "wcl.so", RTLD_LAZY);
	}
	if (!dl_handle) {
		fprintf(stderr, ERROR "ntdcmsmac open fail:%s\n", dlerror());
		exit(1);
	}

	DllInterface.NTDCMS = dlsym(dl_handle, "NTDCMS");
	DllInterface.InitNTDCMS = dlsym(dl_handle, "InitNTDCMS");
	DllInterface.CreateNTDCMS = dlsym(dl_handle, "CreateNTDCMS");
	DllInterface.EndNTDCMS = dlsym(dl_handle, "EndNTDCMS");
	DllInterface.StartColorAdjust = dlsym(dl_handle, "StartColorAdjust");
	DllInterface.StartAdjustJob = dlsym(dl_handle, "StartAdjustJob");
	DllInterface.UpdateScreenThread = dlsym(dl_handle, "UpdateScreenThread");
	if (DllInterface.UpdateScreenThread == NULL) {
		printf("pfIpdateScreenThread == NULL");
	}
	DllInterface.UpdateScreenTRC = dlsym(dl_handle, "UpdateScreenTRC");
	DllInterface.UpdateScreenTonerTRC = dlsym(dl_handle, "UpdateScreenTonerTRC");
}

int main(int argc, char *argv[])
{
	const char *const job_id = argv[1];
	const char *const job_user = argv[2];
	const char *const job_title = argv[3];
	const char *const num_copies = argv[4];
	const char *const job_options = argv[5];
	const char *const filename = argv[6];

	signal(SIGPIPE, SIGPIPE_handler);

	char tmpfilename[1024];
	int cups_temp_fd = cupsTempFd(tmpfilename, sizeof (tmpfilename));

	signal(SIGALRM, SIGALRM_handler);
	struct itimerval value = {
		.it_value = { .tv_sec = 25, },
		.it_interval = { .tv_sec = 25, },
	};
	struct itimerval ovalue;
	setitimer(0, &value, &ovalue);
	printlog(INFO CKP "001");
	printlog(INFO CKP "002");

	setbuf(stderr, NULL);
	printlog(INFO CKP "003");

	if (argc != 6 && argc != 7) {
		//TODO out
		printlog(INFO CKP "004");
		exit(1);
	}

	int raster_image_fd = argc == 7 ?
		open_raster_image(filename) :
		(printlog(INFO CKP "005"), 0);

	printlog(INFO " InitDllInterface()...");
	InitDllInterface();
	printlog(INFO CKP "006");

	signal(SIGTERM, SIGTERM_handler);
	printlog(INFO CKP "007");

	cups_option_t *options = NULL;
	int num_options = cupsParseOptions(job_options, 0, &options);

	char *ppdname = getenv("PPD");
	ppd_file_t *ppd = ppdOpenFile(ppdname);
	if (!ppd) {
		printlog(ERROR "Unable to load PPD");
		printlog(INFO CKP "008b");
		exit(1);
	}
	printlog(INFO CKP "008a");

	const char *opt_media = cupsGetOption("media", num_options, options);
	const char *opt_PageSize = cupsGetOption("PageSize", num_options, options);
	const char *opt_PMDuplexing = cupsGetOption("com.apple.print.PrintSettings.PMDuplexing..n.", num_options, options);
	if (!opt_PageSize) {
		if (opt_media) {
			int num_options = cupsAddOption("PageSize", opt_media, 0, &options);
		}
	}
	ppdMarkDefaults(ppd);
	int conflict = cupsMarkOptions(ppd, num_options, options);
	if (conflict) {
		printlog(INFO "Option conflicts exit. Fixing...");
		int conflict_2 = ppdMarkOption(ppd, "MediaType", "Covers");
		if (conflict_2) {
			printlog(ERROR "Option conflicts exit. Unable to fix...");
			exit(1);
		}
		printlog(INFO "Option conflicts exit. Fixing... Done...");
	}
	printlog(INFO CKP "009");

	ppd_choice_t *pageSize = ppdFindMarkedChoice(ppd, "PageSize");
	int PageSize;
	if (pageSize) {
		if (strcmp(pageSize->choice, "Letter") == 0) {
			PageSize = 0;
		} else if (strcmp(pageSize->choice,"A4") == 0) {
			PageSize = 1;
		} else if (strcmp(pageSize->choice,"A5") == 0) {
			PageSize = 2;
		} else if (strcmp(pageSize->choice,"A6") == 0) {
			PageSize = 3;
		} else if (strcmp(pageSize->choice, "B5") == 0) {
			PageSize = 4;
		} else if (strcmp(pageSize->choice, "B6") == 0) {
			//TODO double check is this constant correct
			PageSize = 5;
		} else if (strcmp(pageSize->choice, "Executive") == 0) {
			PageSize = 6;
		} else if (strcmp(pageSize->choice, "16K") == 0) {
			PageSize = 7;
		} else if (strcmp(pageSize->choice, "A5_LEF") == 0) {
			PageSize = 8;
		}
	}

	/* find_DrvResolution */
	ppd_choice_t *drvResolution =  ppdFindMarkedChoice(ppd, "DrvResolution");
	int DrvResolution;
	if (drvResolution) {
		if (strcmp(drvResolution->choice, "600dpi") == 0) {
			DrvResolution = 600;
		} else {
			DrvResolution = 1200;
		}
	}

	FILE *const logf = fopen(LOGFILE, "a+");
	if (logf) {
		//verbose
	}
	fclose(logf);

	printlog(INFO "ppdFindMarkedChoice_DrvDuplex start");
	ppd_choice_t *drvDuplex = ppdFindMarkedChoice(ppd, "DrvDuplex");
	int DrvDuplex = 0;
	if (drvDuplex) {
		char *opt = NULL;
		if (strcasecmp(drvDuplex->choice, "DrvDuplexNoTumble") == 0) {
			DrvDuplex = 1;
			opt = "DrvDuplexNoTumble";
		} else if (strcasecmp(drvDuplex->choice, "DrvDuplexTumble") == 0) {
			DrvDuplex = 2;
			opt = "DrvDuplexTumble";
		}
		if (DrvDuplex != 0) {
			int conflict = ppdMarkOption(ppd, "DrvDuplex", opt);
			if (conflict) {
				printlog(ERROR "Option conflicts exist");
				exit(1);
			}
		}
	}

	printlog(INFO "cupsRasterOpen start");
	cups_raster_t *raster_stream = cupsRasterOpen(raster_image_fd, 0);
	if (!raster_stream) {
		printlog(INFO "cupsRasterOpen returns NULL.");
	}
	printlog(INFO "cupsRasterOpen end");

	ppd_choice_t *collation = ppdFindMarkedChoice(ppd, "Collate");
	int Collation;
	if (collation) {
		if (strcasecmp(collation->choice, "True") == 0) {
			Collation = 1;
		}
	}

	/* cupsRasterHeaderStart */
	printlog(INFO "cupsRasterReadHeader2 start");

	if (DrvDuplex) {
		/* drvDuplexSet */
		printlog("while done");
		//TODO
	}

	cups_page_header2_t hdr; //
	int fail = cupsRasterReadHeader2(raster_stream, &hdr);
	if (fail) {
		/* failReadHeader2 */
	}

	
	
	
}
